#include<iostream>
#include<math.h>
using namespace std;

bool kiemTraSoNguyenTo(int );
bool kiemTraSoNguyenTo(int number)
{
	if(number<=1) return false;
	int x = sqrt(number);
	for(int i=2;i<=x;i++)
	{
		if(number%i == 0) return false;
	}
	return true;
}

int main()
{
	int n;
	cout<<"Nhap so ban muon kiem tra: "; cin>>n;
	if(kiemTraSoNguyenTo(n)) cout<<n<<" la so nguyen to"<<endl;
	else cout<<n<<" khong phai so nguyen to"<<endl;
	system("pause");
	return 0;
}
